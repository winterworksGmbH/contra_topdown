<?xml version="1.0" encoding="UTF-8"?>
<tileset name="grass" tilewidth="8" tileheight="8" tilecount="1024" columns="32">
 <image source="../images/tilesets/grass.png" width="256" height="256"/>
 <terraintypes>
  <terrain name="grass" tile="69"/>
  <terrain name="mud" tile="0"/>
 </terraintypes>
 <tile id="4" terrain=",,,0"/>
 <tile id="5" terrain=",,0,0"/>
 <tile id="6" terrain=",,0,0"/>
 <tile id="7" terrain=",,0,"/>
 <tile id="36" terrain=",0,,0"/>
 <tile id="37" terrain="0,0,0,0"/>
 <tile id="39" terrain="0,,0,"/>
 <tile id="45" terrain="0,0,0,"/>
 <tile id="46" terrain="0,0,,0"/>
 <tile id="68" terrain=",0,,0"/>
 <tile id="71" terrain="0,,0,"/>
 <tile id="77" terrain="0,,0,0"/>
 <tile id="78" terrain=",0,0,0"/>
 <tile id="100" terrain=",0,,"/>
 <tile id="101" terrain="0,0,,"/>
 <tile id="102" terrain="0,0,,"/>
 <tile id="103" terrain="0,,,"/>
 <tile id="132" terrain=",,,1"/>
 <tile id="133" terrain=",,1,1"/>
 <tile id="134" terrain=",,1,1"/>
 <tile id="135" terrain=",,1,"/>
 <tile id="164" terrain=",1,,1"/>
 <tile id="165" terrain="1,1,1,1"/>
 <tile id="167" terrain="1,,1,"/>
 <tile id="173" terrain="1,1,1,"/>
 <tile id="174" terrain="1,1,,1"/>
 <tile id="196" terrain=",1,,1"/>
 <tile id="199" terrain="1,,1,"/>
 <tile id="205" terrain="1,,1,1"/>
 <tile id="206" terrain=",1,1,1"/>
 <tile id="228" terrain=",1,,"/>
 <tile id="229" terrain="1,1,,"/>
 <tile id="230" terrain="1,1,,"/>
 <tile id="231" terrain="1,,,"/>
</tileset>
